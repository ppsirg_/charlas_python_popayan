import os
from cx_Freeze import setup, Executable

# code here

ejecutable = [Executable('game.py')]

build_options = {
    'includes': ['os', 'gi', 'random', 'gi.overrides.Gtk'],
    'packages': ['gi', 'os', 'random'],
    'optimize': 2,
    # 'include_files': ['ui']
}

setup(
    name='busca_ocas',
    version='0.1.0',
    descripcion='juego de buscaminas mezclado con la oca',
    author='pedro rivera',
    author_email='myemail@python.es',
    url='https://mything.com',
    executables=ejecutable,
    options=dict(build_exe=build_options)
)
