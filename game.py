import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from random import randint, choice


class gameMainWindow(Gtk.Window):
    """
    create a game Window
    """

    def __init__(self):
        # create the window with the title
        Gtk.Window.__init__(self, title='mi juego')
        self.widgets = []
        self.mines_number = 20
        self.rows = 5
        self.columns = 10
        self.mines = []
        self.current_position = 0
        # build interface
        self.build_interface()
        self.build_mines()

    def build_mines(self):
        m = [a for a in range(self.columns * self.rows)]
        self.mines = []
        for i in range(self.mines_number):
            self.mines.append(choice(m))
            m.remove(self.mines[-1])

    def build_grid(self):
        """
        build a grid, like a table of buttons for the game to start
        """
        # create a grid
        grid = Gtk.Grid(margin=5)
        grid.props.column_spacing = 6
        grid.props.row_spacing = 4
        # an array to save all buttons we will create in an automatic way
        self.widgets = []
        # a variable to save the number of the button
        number = 1
        # create columns
        for r in range(self.rows):
            self.widgets.append([])
            # create rows
            for c in range(self.columns):
                # create the button with its name and save on widget array
                self.widgets[r].append(Gtk.Label(number))
                # if is the first button, use add method
                if c == 0 and r == 0:
                    grid.add(self.widgets[-1][-1])
                # if is not the first button, use attach methods
                else:
                    grid.attach(self.widgets[-1][-1], c, r, 1, 1)
                # add one to number so number of button changes in one
                number += 1
        # return grid
        return grid

    def build_interface(self):
        """
        build user interface
        """
        # declare ui elements

        # the main box
        self.main_box = Gtk.Box(spacing=10)
        # the box that contains dado label and button
        dado_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        # the grid that contains cells of game
        tablero_grid = self.build_grid()
        # label that shows recent nuber of dices(dados)
        self.dado_label = Gtk.Label('++++')
        # button that allows to launch dices
        self.lanzador_btn = Gtk.Button.new_with_label('lanzar dados')

        # assembly ui
        self.add(self.main_box)  # put main box in the window
        self.main_box.pack_start(dado_box, True, True, 5)
        self.main_box.pack_start(tablero_grid, True, True, 5)
        dado_box.pack_start(self.lanzador_btn, False, True, 5)
        dado_box.pack_start(self.dado_label, False, True, 5)

        # set ui properties
        #self.props.default_width = 800
        # self.props.default_height = 600

        # connect events with handlers
        self.connect("delete-event", Gtk.main_quit)
        self.lanzador_btn.connect('clicked', self.launch_dice)
        self.show_all()

    def launch_dice(self, widget):
        """
        launch a dices
        """
        number = randint(1, 6)
        self.dado_label.set_text('sacaste\n{}'.format(number))
        self.update_board(self.current_position, number)
        self.current_position += number
        if self.current_position in self.mines:
            d = Gtk.MessageDialog(
                self, 0,
                Gtk.MessageType.INFO,
                Gtk.ButtonsType.OK,
                'bang on {}!!, you lost'.format(self.current_position + 1)
                )
            d.run()
            d.destroy()

    def update_board(self, current, number):
        # put number in current
        i = int(self.current_position / self.columns)
        j = self.current_position % self.columns
        self.widgets[i][j].set_text(str(self.current_position + 1))
        # put cat in new
        new = self.current_position + number
        i = int(new / self.columns)
        j = new % self.columns
        self.widgets[i][j].set_text('(:v)') # this (:v) represents the player


def main():
    # create a window
    game = gameMainWindow()
    # tell graphic library to build and show all
    Gtk.main()


if __name__ == '__main__':
    # activate the program
    main()
