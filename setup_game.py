from distutils.core import setup
# code here

setup(
    name='busca_ocas',
    version='0.1.0',
    descripcion='juego de buscaminas mezclado con la oca',
    author='pedro rivera',
    author_email='myemail@python.es',
    url='https://mything.com',
    packages=['distutils', 'distutils.command', 'gi']
)
